#!/bin/bash

function init_build(){
  export CI_APPLICATION_REPOSITORY="$CI_REGISTRY/app"
  export CI_COMMIT_SHORT_SHA="${CI_COMMIT_SHA:0:8}"
  export CI_APPLICATION_TAG="${CI_COMMIT_SHORT_SHA}"
  export CI_CONTAINER_NAME="ci_job_build_${CI_JOB_ID}"
}
